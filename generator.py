
import glob
import numpy as np
import string
import numpy.random
from PIL import Image, ImageFont, ImageDraw
from PIL.Image import LANCZOS
from keras.preprocessing.image import img_to_array, array_to_img
from skimage.io import imread
from skimage.util import view_as_windows

NOISE_PATH = "resources/noise_sources/*.jpg"

TRAINING_PATH = "resources/train/"
TRAINING_LABEL = "resources/train.csv"

VALIDATION_PATH = "resources/validation/"
VALIDATION_LABEL = "resources/validation.csv"

TESTING_PATH = "resources/tester/"
TESTING_LABEL = "resources/tester.csv"

number_of_images = 30000  # This is 3/4 of the total number of images.
number_of_chars = 1

characters_list = list(string.digits) + list(string.ascii_letters)
characters = string.digits + string.ascii_letters


class generator:
    # TODO: Strongly consider using a config file all of the parameters.

    #########################################################################

    #   Creates a list of chars or strings for text in generated images for
    # training the network. This is likely going to use a config file.
    def image_text_writer(size):
        string_size = list(np.random.randint(1, size=size))
        for i in string_size:
            generated_string = ''.join(np.random.choice(
                characters_list, number_of_chars))

        return generated_string

    def generate_images(number_of_images, image_path, image_label, save_flag=False):
        generated_string = generator.image_text_writer(number_of_images)
        data.make_new_data(generated_string, image_path,
                           image_label, save_flag)

    def generate_write():
        generator.generate_images(
            number_of_images, TRAINING_PATH, TRAINING_LABEL, True)
        generator.generate_images(number_of_images // 3, VALIDATION_PATH,
                                  VALIDATION_LABEL, True)

        test_write()
        print("Completed Image Generation")

    def generate_no_write(number=number_of_images):

        generator.generate_images(number, TRAINING_PATH, TRAINING_LABEL, False)
        generator.generate_images(
            number // 3, VALIDATION_PATH, VALIDATION_LABEL, False)

        print("Passed Images to Network")
        test_write()

        return test_images, test_labels, val_images, val_labels

    #########################################################################

    def generate_locator_images(number):
        generated_string = create_image.image_text_writer(number)
        split_images, split_text = data.get_locator(generated_string)
        return split_images, split_text

    def test_write():
        generator.generate_images(1, TESTING_PATH, TESTING_LABEL, True)


class data:

    # Different font arrays for added randomness. I might have to add more.
    # Ideally, this will grab a list of all installed fonts on the system.
    # Installed a couple extra fonts myself, one may want to change these lists.

    # fonts = ["LiberationMono-Regular.ttf", "LiberationSerif-Regular.ttf",
    #         "Roboto-Bold.ttf", "DejaVuSans.ttf", "NotoMono-Regular.ttf",
    #         "FreeMono.ttf", "FreeSans.ttf", "FreeSerif.ttf", "Ubuntu-LI.ttf",
    #         "UbuntuMono-R.ttf", "NotoSerif-Bold.ttf", "Ubuntu-B.ttf",
    #         "A_Sensible_Armadillo.ttf", "Adler.ttf", "TravelingTypewriter.ttf",
    #         "LiberationSans-Italic.ttf", "LiberationSerif-Italic.ttf",
    #         "Quirlycues.ttf"]

    # fonts2 = ["LiberationSans-Bold.ttf", "LiberationSerif-Regular.ttf",
    #         "Roboto-Thin.ttf", "DejaVuSerif.ttf", "NotoSans-Regular.ttf",
    #         "NotoSerif-Regular.ttf", "FreeSans.ttf", "Ubuntu-R.ttf",
    #         "UbuntuMono-R.ttf", "FiraCode-Regular.ttf", "Ubuntu-B.ttf",
    #         "alphabetized_cassette_tapes.ttf", "attack_of_the_cucumbers.ttf",
    #         "Quikhand.ttf", "TravelingTypewriter.ttf", "Quirlycues.ttf",
    #         "NotoSans-Bold.ttf"]

    fonts_all = ["LiberationMono-Regular.ttf", "LiberationSans-Bold.ttf",
                 "LiberationSerif-Regular.ttf", "Roboto-Bold.ttf",
                 "Roboto-Thin.ttf", "DejaVuSans.ttf", "DejaVuSerif.ttf",
                 "NotoMono-Regular.ttf", "NotoSans-Regular.ttf",
                 "NotoSerif-Regular.ttf", "FreeMono.ttf", "FreeSans.ttf",
                 "FreeSerif.ttf", "Ubuntu-R.ttf", "Ubuntu-LI.ttf",
                 "UbuntuMono-R.ttf", "FiraCode-Regular.ttf", "NotoSerif-Bold.ttf",
                 "Ubuntu-B.ttf", "A_Sensible_Armadillo.ttf", "Adler.ttf",
                 "attack_of_the_cucumbers.ttf", "Quikhand.ttf",
                 "TravelingTypewriter.ttf", "LiberationSans-Italic.ttf",
                 "LiberationSerif-Italic.ttf", "NotoSans-Bold.ttf"]

    fonts_mono = ["LiberationMono-Regular.ttf", "NotoMono-Regular.ttf",
                  "FreeMono.ttf", "UbuntuMono-R.ttf", "FiraCode-Regular.ttf"]
    fonts_test = ["LiberationMono-Regular.ttf"]

    image_size = (32, 32)
    text_size = 32

    ########################################################################

    #  REWRITE THIS SECTION ITS NOT WORKING. IMAGES HAVE NO NAMES. NAMES ARE
    #  IN

    #   From the random char sequence, individual images are made and returned
    def make_new_data(generated_string, image_path, label_path, save_to_disk):

        labels = []      # Text in image
        csv_labels = []  # Text in csv for image and label
        all_images = []  # All images made
        for i, image_text in enumerate(generated_string):
            image = create_image.default_image(image_text, data.image_size)
            all_images.append(image)
            labels.append(image_text)

            if save_to_disk:
                # 1.png, c   --- <filename>, <image text> for the CSV
                image.save(image_path + str(i) + '.png')
                csv_labels.append(str(i) + '.png,' + image_text)

        if save_to_disk:
            with open(label_path, mode='w') as File:
                File.write('\n'.join(csv_labels))

        return all_images, labels
    # MAKE SAVE THE FULL FEATURED THING, GET IS JUST SAVE WITH SAVE DISABLED FOR RETURN

    ##########################################################################

    #   Generates text images for a locator to find text
    def get_locator_text(generated_string):
        labels = []
        all_images = []
        for i, image_text in enumerate(generated_string):
            image = create_image.get_image(
                image_text, fonts_mono, data.image_size,
                rand.text_size, rand.colour(0, 0),
                rand.colour(100, 256))
            all_images.append(image)
            labels.append("1")
        return all_images, labels

    def get_locator_noise():
        images = []
        labels = []
        for image_path in glob.glob(NOISE_PATH):
            image = imread(image_path)[:, :, :3]
            image = img_to_array(image)

            # Rather than pass array after array back,
            # I'll append to the referrence
            subimages = create_image.image_splitter(image)
            i, o = 0, 0
            for i in range(len(subimages)):
                for o in range(len(subimages[i])):
                    new_img = array_to_img(subimages[i][o][0],
                                           data_format='channels_last')

                    images.append(new_img)
                    labels.append("0")

        return images, labels

    #   Generates noise for NN to find text in the photo
    def get_locator(generated_string):
        all_images, all_labels = [], []

        text_images, text_labels = get_locator_text(generated_string)
        noise_images, noise_labels = create_image.get_noise()

        all_images = text_images + noise_images
        all_labels = text_labels + noise_labels

        return all_images, all_labels

    ###########################################################################


class rand:

    def random(min=0, max=256):
        return np.random.random_integers(min, max)

    def colour(mini=0, maxi=256):
        return (rand.random(mini, maxi),
                rand.random(mini, maxi),
                rand.random(mini, maxi))

    def image_size(size, variation):
        width = size[0]
        height = size[1]
        return (rand.random(width-variation, width+variation),
                rand.random(height - variation, height + variation))

    def text_size(size=data.text_size, variation=2):
        return rand.random(size - variation, size + variation)

    ##########################################################################
    ##########################################################################


class create_image:

    def get_image(text, fonts, size, font_size, bg_colour, text_colour):
        image = Image.new('RGB', size, bg_colour)
        draw = ImageDraw.Draw(image)

        font = create_image.get_font(fonts, font_size)
        max_size = font.getsize(text)  # width, height of font

        offset = ((size[0] - max_size[0]) / 2,
                  (size[1] - max_size[1]) / 2)  # centre

        draw.text(offset, text, text_colour, font)

        return image

    ###########################################################################
    #   Default properties. Create_image has more customization availible.
    def default_image(image_text, size):

        background = rand.colour(0, 0)
        foreground = rand.colour(100, 256)

        bad_sized_image = rand.image_size(
            size=data.image_size, variation=2)

        image = create_image.get_image(
            image_text, data.fonts_test,
            bad_sized_image, 28, background, foreground)

        return create_image.scale_to_size(image, size)

    ###########################################################################

    def image_splitter(image):
        return view_as_windows(image, (32, 32, 3), step=32)

    def scale_to_size(image, size):
        return image.resize(size, resample=LANCZOS)

    def get_font(fonts, size):
        font_path = ''.join(np.random.choice(fonts, 1))
        # return ImageFont.truetype(font_path, size=size)
        return ImageFont.truetype(font=font_path, size=size)

    def get_font_test():
        font_path = ''.join(np.random.choice(data.fonts_test, 1))
        print(font_path)


if __name__ == "__main__":
    create_image.get_font_test()
    generator.test_write()
