# ML-Reader

This is a machine learning project made to take an image or photo as an input and it will try to accurately find and present text within the photo to the user as a string output. This may have use as a page scanner, tool for the visually impaired.

The project is built using Python 3. For my own dev environment I'm using an AMD GPU, therefore I'm running AMD's rocm/tensorflow docker image.

