FROM tensorflow/tensorflow:latest-py3
#FROM rocm/tensorflow:latest
COPY . /ml-reader

WORKDIR /ml-reader
ADD fonts/ /usr/share/fonts/
RUN chmod 644 -R /usr/share/fonts/
RUN yes | apt install fontconfig && fc-cache -fv
RUN pip3 install -r requirements.txt
CMD ["bash"]
